
package Modelo;

public class Producto {
    
    private int codigo;
    private String descripcion;
    private String unidadMedida;
    private float precioCompra;
    private float precioVenta;
    private int cantidadProducto;
    
    //public Producto();
    //public Producto(Producto);
    
    public Producto(int codigo, String descripcion, String unidadMedida, float precioCompra, float precioVenta, int cantidadProducto) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.unidadMedida = unidadMedida;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
        this.cantidadProducto = cantidadProducto;
    }

    public Producto(Producto otro) {
        this.codigo = otro.codigo;
        this.descripcion = otro.descripcion;
        this.unidadMedida = otro.unidadMedida;
        this.precioCompra = otro.precioCompra;
        this.precioVenta = otro.precioVenta;
        this.cantidadProducto = otro.cantidadProducto;
    }
    
    public Producto() {
        this.codigo = 0;
        this.descripcion = "";
        this.unidadMedida = "";
        this.precioCompra = 0;
        this.precioVenta = 0;
        this.cantidadProducto = 0;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(int cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }
    
    public float calcularPrecioVenta() {
        return getPrecioVenta() * getCantidadProducto();
    }
    public float calcularPrecioCompra() {
        return getPrecioCompra() * getCantidadProducto();
    }
    public float calcularGanancia() {
        return calcularPrecioVenta() - calcularPrecioCompra();
    }
    
}
