/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Modelo.Producto;
import Vista.dlgProducto;
// Escuchar eventos
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
// Para vista
import javax.swing.JFrame;
import javax.swing.JOptionPane;


/**
 *
 * @author TM16
 */
public class Controlador implements ActionListener {
    
    private Producto pro;
    private dlgProducto vista;

    public Controlador(Producto pro, dlgProducto vista) {
        this.pro = pro;
        this.vista = vista;
    
        // Hacer que el controlador escuche los botones de la vista
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);

        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
    
    }
    
    private void iniciarVista() {
        vista.setTitle(":: Productos ::");
        vista.setSize(500, 500);
        vista.setVisible(true);
        
    }
    
    public static void main(String[] args) {
        Producto pro = new Producto();
        dlgProducto vista = new dlgProducto(new JFrame(), true);
        
        Controlador contra = new Controlador(pro, vista);
        contra.iniciarVista();
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        // Nuevo --> When you press it, you can save and show
        if(e.getSource() == vista.btnNuevo) {
            // Enable inputs
            vista.txtCantidad.setEnabled(true);
            vista.txtCodigo.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.txtPrecioCompra.setEnabled(true);
            vista.txtPrecioVenta.setEnabled(true);
            vista.txtUnidadMedida.setEnabled(true);

            // Enable guardar, buttt, you gotta have true data, so you can show data
            vista.btnGuardar.setEnabled(true);
            
            
            // Clean variables
            vista.txtCantidad.setText("");
            vista.txtCodigo.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPrecioCompra.setText("");
            vista.txtPrecioVenta.setText("");
            vista.txtTotalCompra.setText("");
            vista.txtTotalVenta.setText("");
            vista.txtTotalGanancia.setText("");
            vista.txtUnidadMedida.setText("");
            
        }
        
        // Guardar --> Locks the data (by now)
        if(e.getSource() == vista.btnGuardar) {
            if(
                // it completes if all the elements aren't empty, something like "isEmpty()" function
                // Java didn't have any isNumber, so I created a public function to check it
                isNumber(vista.txtCodigo.getText()) &&
                isNumber(vista.txtCantidad.getText()) &&
                !vista.txtDescripcion.getText().equals("") &&
                isNumber(vista.txtPrecioCompra.getText()) &&
                isNumber(vista.txtPrecioVenta.getText()) &&
                !vista.txtUnidadMedida.getText().equals("")
                )
            {
                // We save the data on the variables
                System.out.println("Data saved...");
                pro.setCodigo(Integer.parseInt(vista.txtCodigo.getText()));
                pro.setDescripcion(vista.txtDescripcion.getText());
                pro.setCantidadProducto(Integer.parseInt(vista.txtCantidad.getText()));
                pro.setPrecioCompra(Float.parseFloat(vista.txtPrecioCompra.getText()));
                pro.setPrecioVenta(Float.parseFloat(vista.txtPrecioVenta.getText()));
                pro.setUnidadMedida(vista.txtUnidadMedida.getText());
                
                // We lock the data
                vista.txtCantidad.setEnabled(false);
                vista.txtCodigo.setEnabled(false);
                vista.txtDescripcion.setEnabled(false);
                vista.txtPrecioCompra.setEnabled(false);
                vista.txtPrecioVenta.setEnabled(false);
                vista.txtUnidadMedida.setEnabled(false);
                
                // We let the user calculate earnings
                vista.btnMostrar.setEnabled(true);
                
            }
        }
        
        // Mostrar --> Calculates the last buttons (earnings, ...)
        if(e.getSource() == vista.btnMostrar) {
            vista.txtTotalCompra.setText(Float.toString(pro.calcularPrecioCompra()));
            vista.txtTotalVenta.setText(Float.toString(pro.calcularPrecioVenta()));
            vista.txtTotalGanancia.setText(Float.toString(pro.calcularGanancia()));
        }
        
        // Limpiar --> Delete data
        if(e.getSource() == vista.btnLimpiar) {
            // Restets input
            vista.txtCantidad.setText("");
            vista.txtCodigo.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPrecioCompra.setText("");
            vista.txtPrecioVenta.setText("");
            vista.txtTotalCompra.setText("");
            vista.txtTotalVenta.setText("");
            vista.txtTotalGanancia.setText("");
            vista.txtUnidadMedida.setText("");
            
            // Resets variables of pro
            pro.setCodigo(0);
            pro.setDescripcion("");
            pro.setCantidadProducto(0);
            pro.setPrecioCompra(0);
            pro.setPrecioVenta(0);
            pro.setUnidadMedida("");
            
        }
        
        // Cancelar --> Cleans the data, and unables save and show
        if(e.getSource() == vista.btnCancelar) {
            // Resets buttons
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            
            // Restets input
            vista.txtCantidad.setText("");
            vista.txtCodigo.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPrecioCompra.setText("");
            vista.txtPrecioVenta.setText("");
            vista.txtTotalCompra.setText("");
            vista.txtTotalVenta.setText("");
            vista.txtTotalGanancia.setText("");
            vista.txtUnidadMedida.setText("");
            
            // Resets variables of pro
            pro.setCodigo(0);
            pro.setDescripcion("");
            pro.setCantidadProducto(0);
            pro.setPrecioCompra(0);
            pro.setPrecioVenta(0);
            pro.setUnidadMedida("");
            
            // We unable inputs
            vista.txtCantidad.setEnabled(false);
            vista.txtCodigo.setEnabled(false);
            vista.txtDescripcion.setEnabled(false);
            vista.txtPrecioCompra.setEnabled(false);
            vista.txtPrecioVenta.setEnabled(false);
            vista.txtUnidadMedida.setEnabled(false);
            
        }
        
        // Cerrar --> Closes the app
        if(e.getSource() == vista.btnCerrar) {
            System.exit(0);
        }
    }
    
    public static boolean isNumber(String str) {
        try {
            // if it CAN turn into a number, we know it's a number
            Integer.parseInt(str);
            return true;
        }
        catch(NumberFormatException e) {
            // otherwise it's not a number
            return false;
        }
    }
    
}
